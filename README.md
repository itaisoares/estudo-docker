docker

# mostrar client, engine
docker version


#numero de containers imagens etc...
docker info


#iniciar o primeiro container
#		             iterativo 	    --publish local:exposto do container    nome imagem
docker container run -it            -p        80:80                         nginx
#  - mostrar rodando localhost (o log a cada refresh)
#  - ir ao docker procurar o nginx, mostrar o dockerfile, que info tão
#    la para variaveis etc

# como listar todos os containers
docker container ls -a   | docker ps -a

# remover um container
docker container rm id

#listar imagens
docker images

#remover imagem
docker images rm id

#rodar de novo mas em background e dando nome a imagem
#                    (detach)
docker container run -d     -p 8080:80 --name mynginx




#criar um container apache
docker container run -d -p 8081:80 --name myapache httpd


#exemplo de uso de variaveis procurar o  mysql 
docker container run -d -p 3306:3306 --name mysql --env MYSQL_ROOT_PASSWORD=123456 mysql

#parar um container
docker container stop <name>  | docker stop <name>

#mostrar
docker ps -a

#remover container em execucao
docker container rm myapache

#forcar remover
docker container rm myapache -f


#entrar no container
docker container exec -it mynginx bash
  - cd usr/share/nginx/html/

exit


#mapear o diretorio
docker container run -d -p 8080:80 -v $(pwd):/usr/share/nginx/html --name mynginx nginx


#colocar o html e mostrar 
    <!DOCKTYPE html>
    <html lang="en">
    
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Hello World</title>
    </head>
    
    <body>
        <h1>Hello World</h1>
    </body>



#mostrar colocando um about.html

#criar um Dockerfile
    FROM nginx:latest
    
    WORKDIR /usr/share/nginx/html
    
    COPY . .


#build no Dockerfile
docker image build -t itaisoares/nginx-website .

#mostrar as imagens
docker images


#criar container
docker run -d -p 8082:80 itaisoares/nginx-website

#push da imagem
docker push itaisoares/nginx-website




#criar um Dockerfile do projeto

FROM node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]


#build da imagem
docker image build -t docker-node-mongo .


#listar um network
docker network ls

#criar um network
docker network create --driver bridge node-mongo

#remover um network
docker network rm <id>

#rodar o mongo
docker container run --network=node-mongo -d -p 27017:27017 --name mongo mongo

#rodar o node
docker container run --network=node-mongo -d -p 80:3000 --name nodeapp docker-node-mongo 




#simplificar usando docker composer

#criar docker-compose.yml

version: '3'
services:
  nodeapp:
    container_name: docker-node-mongo
    restart: always
    build: .
    ports:
      - '80:3000'
    links:
      - mongodb
  mongodb:
    container_name: mongo
    image: mongo
    ports:
      - '27017:27017'


#subir o docker compose

docker-compose up

docker-compose up -d 

docker-compose down

#especificar o network
#  networks:
#     - nomeNetwork



